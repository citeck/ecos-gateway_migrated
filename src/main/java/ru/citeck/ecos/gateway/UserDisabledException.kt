package ru.citeck.ecos.gateway

class UserDisabledException(msg: String) : RuntimeException(msg)
