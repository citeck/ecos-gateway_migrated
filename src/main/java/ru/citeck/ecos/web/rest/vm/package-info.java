/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.citeck.ecos.web.rest.vm;
